# FreeTypePhp

It is a PHP wrapper to some [FreeType](https://www.freetype.org/) and [HarfBuzz](https://github.com/behdad/harfbuzz/wiki) library functions.
All methods are using the FT_ENCODING_UNICODE, so all input and output char codes are unicode values.

## Installation

FreeType2 and HarfBuzz libraries are required for this extension. 
For Ubuntu use the command below (to be honest, I don't know which of them are really required):  
 
    sudo apt-get install libfreetype6 libfreetype6-dev gir1.2-harfbuzz-0.0 libharfbuzz-dev libharfbuzz-gobject0 libharfbuzz-icu0 libharfbuzz0b libglib2.0-dev libcairo2-dev libicu-dev libharfbuzz-icu0-dbgsym
    
If you have Zephir installed, just run the next code:

	cd freetypephp
	zephir build
	
If you have no Zephir installation, use the phpize utility (prepared only for php 7.0):

	cd freetypephp/ext
	phpize
	./configure
	make && make install
	
To add extension to your php.ini files, run `./install-so.sh` script (for default Ubuntu installation of php-7.0 only).  

## Usage

	<?php
	
	// Initialize font
	$font = new \FreeTypePhp\Font('/path/to/font.ttf');
	
	// Do some actions with font instance
	$availableChars = $font->getAllChars();
	
## Available methods

### \FreeTypePhp\Font::__construct(string $path)

Font constructor. Requires a valid path to the font file.

	$font = new \FreeTypePhp\Font('/path/to/font.ttf');
	
### \FreeTypePhp\Font::getAllChars() :array

Get all chars containing in the font file.
Returns an associative array in format array(char_code => glyph_index);
	
	$availableChars = $font->getAllChars();
	$charCode = ord('A');
	
	if (isset($availableChars[$charCode])) {
		echo 'A is OK'.
	} else {
		echo 'Unknown char';
	}
	
### \FreeTypePhp\Font::getCharIndex(int $charCode) :int

Get glyph index of given char code in the font. Returns 0 if char does not exist in the font char map.

	if ($font->getCharIndex(65)) {
		echo 'A is OK';
	} else {
		echo 'We lost A';
	}
	
### \FreeTypePhp\Font::getFamilyName() :string

Get font family name.

	echo 'Font family name: ' . $font->getFamilyName(); 
	
### \FreeTypePhp\Font::getStyleName() :string

Get font style name (e. g. 'Regular')

	echo 'Font style name: ' . $font->getStyleName(); 
	
### \FreeTypePhp\Font::getStringGlyphs(string $inputString, string $langCode, string $textScript = null, string $textDirection = null) :array

Get glyph indexes needed to render the string by the font.

Method params:
- string $inputString: string to process
- string $langCode: language code (e. g. 'en'; see [source code](https://github.com/behdad/harfbuzz/blob/master/src/hb-ot-tag.cc) for languages list)
- string $textScript: script code (e. g. 'Arab'; default 'Latn'; see [source code](https://github.com/behdad/harfbuzz/blob/master/src/hb-common.h) for scripts list)
- string $textDirection: text direction ('ltr' (by default), 'rtl', 'ttb', 'btt')

Returns an array in format array(glyph_index => glyph_index) to avoid duplicates.

	echo implode(',', $font->getStringGlyphs('A+B', 'en')); // 34,12,35 for example
	
### \FreeTypePhp\Font::getFileGlyphs(string $filePath, string $langCode, string $textScript = null, string $textDirection = null) :array

Get glyph indexes needed to render texts from file by current font.

Method params:
- string $filePath: path to text file to process
- other params are identical to params in \FreeTypePhp\Font::getStringGlyphs()

Returns an array in format array(glyph_index => glyph_index) to avoid duplicates.

	echo count($font->getFileGlyphs('/path/to/file.txt'), 'ar', \FreeTypePhp\HarfBuzz::SCRIPT_ARABIC, \FreeTypePhp\HarfBuzz::DIRECTION_RTL); // 82 for example
	
## HarfBuzz class

\FreeTypePhp\HarfBuzz is a helper class with script codes and text directions constants. 
See class source code for more info, because there are a lot of constants inside.