
/* This file was generated automatically by Zephir do not modify it! */

#ifndef PHP_FREETYPEPHP_H
#define PHP_FREETYPEPHP_H 1

#ifdef PHP_WIN32
#define ZEPHIR_RELEASE 1
#endif

#include "kernel/globals.h"

#define PHP_FREETYPEPHP_NAME        "freetypephp"
#define PHP_FREETYPEPHP_VERSION     "0.1.1"
#define PHP_FREETYPEPHP_EXTNAME     "freetypephp"
#define PHP_FREETYPEPHP_AUTHOR      "gitlab.com/kukymbr"
#define PHP_FREETYPEPHP_ZEPVERSION  "0.9.6a-dev-aef205594b"
#define PHP_FREETYPEPHP_DESCRIPTION "PHP wrapper to some FreeType and HarfBuzz library functions"



ZEND_BEGIN_MODULE_GLOBALS(freetypephp)

	int initialized;

	/* Memory */
	zephir_memory_entry *start_memory; /**< The first preallocated frame */
	zephir_memory_entry *end_memory; /**< The last preallocate frame */
	zephir_memory_entry *active_memory; /**< The current memory frame */

	/* Virtual Symbol Tables */
	zephir_symbol_table *active_symbol_table;

	/** Function cache */
	HashTable *fcache;

	zephir_fcall_cache_entry *scache[ZEPHIR_MAX_CACHE_SLOTS];

	/* Cache enabled */
	unsigned int cache_enabled;

	/* Max recursion control */
	unsigned int recursive_lock;

	
ZEND_END_MODULE_GLOBALS(freetypephp)

#ifdef ZTS
#include "TSRM.h"
#endif

ZEND_EXTERN_MODULE_GLOBALS(freetypephp)

#ifdef ZTS
	#define ZEPHIR_GLOBAL(v) ZEND_MODULE_GLOBALS_ACCESSOR(freetypephp, v)
#else
	#define ZEPHIR_GLOBAL(v) (freetypephp_globals.v)
#endif

#ifdef ZTS
	void ***tsrm_ls;
	#define ZEPHIR_VGLOBAL ((zend_freetypephp_globals *) (*((void ***) tsrm_get_ls_cache()))[TSRM_UNSHUFFLE_RSRC_ID(freetypephp_globals_id)])
#else
	#define ZEPHIR_VGLOBAL &(freetypephp_globals)
#endif

#define ZEPHIR_API ZEND_API

#define zephir_globals_def freetypephp_globals
#define zend_zephir_globals_def zend_freetypephp_globals

extern zend_module_entry freetypephp_module_entry;
#define phpext_freetypephp_ptr &freetypephp_module_entry

#endif
