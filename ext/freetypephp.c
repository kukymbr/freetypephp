
/* This file was generated automatically by Zephir do not modify it! */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <php.h>

#include "php_ext.h"
#include "freetypephp.h"

#include <ext/standard/info.h>

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/globals.h"
#include "kernel/main.h"
#include "kernel/fcall.h"
#include "kernel/memory.h"



zend_class_entry *freetypephp_exception_ce;
zend_class_entry *freetypephp_font_ce;
zend_class_entry *freetypephp_harfbuzz_ce;

ZEND_DECLARE_MODULE_GLOBALS(freetypephp)

PHP_INI_BEGIN()
	
PHP_INI_END()

static PHP_MINIT_FUNCTION(freetypephp)
{
	REGISTER_INI_ENTRIES();
	ZEPHIR_INIT(FreeTypePhp_Exception);
	ZEPHIR_INIT(FreeTypePhp_Font);
	ZEPHIR_INIT(FreeTypePhp_HarfBuzz);
	return SUCCESS;
}

#ifndef ZEPHIR_RELEASE
static PHP_MSHUTDOWN_FUNCTION(freetypephp)
{
	zephir_deinitialize_memory(TSRMLS_C);
	UNREGISTER_INI_ENTRIES();
	return SUCCESS;
}
#endif

/**
 * Initialize globals on each request or each thread started
 */
static void php_zephir_init_globals(zend_freetypephp_globals *freetypephp_globals TSRMLS_DC)
{
	freetypephp_globals->initialized = 0;

	/* Memory options */
	freetypephp_globals->active_memory = NULL;

	/* Virtual Symbol Tables */
	freetypephp_globals->active_symbol_table = NULL;

	/* Cache Enabled */
	freetypephp_globals->cache_enabled = 1;

	/* Recursive Lock */
	freetypephp_globals->recursive_lock = 0;

	/* Static cache */
	memset(freetypephp_globals->scache, '\0', sizeof(zephir_fcall_cache_entry*) * ZEPHIR_MAX_CACHE_SLOTS);


}

/**
 * Initialize globals only on each thread started
 */
static void php_zephir_init_module_globals(zend_freetypephp_globals *freetypephp_globals TSRMLS_DC)
{

}

static PHP_RINIT_FUNCTION(freetypephp)
{

	zend_freetypephp_globals *freetypephp_globals_ptr;
#ifdef ZTS
	tsrm_ls = ts_resource(0);
#endif
	freetypephp_globals_ptr = ZEPHIR_VGLOBAL;

	php_zephir_init_globals(freetypephp_globals_ptr TSRMLS_CC);
	zephir_initialize_memory(freetypephp_globals_ptr TSRMLS_CC);


	return SUCCESS;
}

static PHP_RSHUTDOWN_FUNCTION(freetypephp)
{
	
	zephir_deinitialize_memory(TSRMLS_C);
	return SUCCESS;
}

static PHP_MINFO_FUNCTION(freetypephp)
{
	php_info_print_box_start(0);
	php_printf("%s", PHP_FREETYPEPHP_DESCRIPTION);
	php_info_print_box_end();

	php_info_print_table_start();
	php_info_print_table_header(2, PHP_FREETYPEPHP_NAME, "enabled");
	php_info_print_table_row(2, "Author", PHP_FREETYPEPHP_AUTHOR);
	php_info_print_table_row(2, "Version", PHP_FREETYPEPHP_VERSION);
	php_info_print_table_row(2, "Build Date", __DATE__ " " __TIME__ );
	php_info_print_table_row(2, "Powered by Zephir", "Version " PHP_FREETYPEPHP_ZEPVERSION);
	php_info_print_table_end();

	DISPLAY_INI_ENTRIES();
}

static PHP_GINIT_FUNCTION(freetypephp)
{
	php_zephir_init_globals(freetypephp_globals TSRMLS_CC);
	php_zephir_init_module_globals(freetypephp_globals TSRMLS_CC);
}

static PHP_GSHUTDOWN_FUNCTION(freetypephp)
{

}


zend_function_entry php_freetypephp_functions[] = {
ZEND_FE_END

};

zend_module_entry freetypephp_module_entry = {
	STANDARD_MODULE_HEADER_EX,
	NULL,
	NULL,
	PHP_FREETYPEPHP_EXTNAME,
	php_freetypephp_functions,
	PHP_MINIT(freetypephp),
#ifndef ZEPHIR_RELEASE
	PHP_MSHUTDOWN(freetypephp),
#else
	NULL,
#endif
	PHP_RINIT(freetypephp),
	PHP_RSHUTDOWN(freetypephp),
	PHP_MINFO(freetypephp),
	PHP_FREETYPEPHP_VERSION,
	ZEND_MODULE_GLOBALS(freetypephp),
	PHP_GINIT(freetypephp),
	PHP_GSHUTDOWN(freetypephp),
	NULL,
	STANDARD_MODULE_PROPERTIES_EX
};

#ifdef COMPILE_DL_FREETYPEPHP
ZEND_GET_MODULE(freetypephp)
#endif
