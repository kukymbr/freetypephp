
#ifdef HAVE_CONFIG_H
#include "../ext_config.h"
#endif

#include <php.h>
#include "../php_ext.h"
#include "../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"
#include "kernel/file.h"
#include "kernel/exception.h"
#include "kernel/memory.h"
#include "kernel/fcall.h"
#include "kernel/concat.h"
#include "kernel/object.h"
#include "ext/spl/spl_exceptions.h"
#include "kernel/operators.h"


/**
 * TrueType Font processing class
 */
ZEPHIR_INIT_CLASS(FreeTypePhp_Font) {

	ZEPHIR_REGISTER_CLASS(FreeTypePhp, Font, freetypephp, font, freetypephp_font_method_entry, 0);

	/**
	 * Path to font file
	 */
	zend_declare_property_null(freetypephp_font_ce, SL("_path"), ZEND_ACC_PROTECTED TSRMLS_CC);

	return SUCCESS;

}

/**
 * Constructor
 *
 * @param string path Path to font file
 */
PHP_METHOD(FreeTypePhp_Font, __construct) {

	int ZEPHIR_LAST_CALL_STATUS;
	zval *path_param = NULL, _0$$3;
	zval path, _1$$3;
	ZEPHIR_INIT_THIS();

	ZVAL_UNDEF(&path);
	ZVAL_UNDEF(&_1$$3);
	ZVAL_UNDEF(&_0$$3);

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 1, 0, &path_param);

	if (unlikely(Z_TYPE_P(path_param) != IS_STRING && Z_TYPE_P(path_param) != IS_NULL)) {
		zephir_throw_exception_string(spl_ce_InvalidArgumentException, SL("Parameter 'path' must be a string") TSRMLS_CC);
		RETURN_MM_NULL();
	}
	if (likely(Z_TYPE_P(path_param) == IS_STRING)) {
		zephir_get_strval(&path, path_param);
	} else {
		ZEPHIR_INIT_VAR(&path);
		ZVAL_EMPTY_STRING(&path);
	}


	if (!((zephir_file_exists(&path TSRMLS_CC) == SUCCESS))) {
		ZEPHIR_INIT_VAR(&_0$$3);
		object_init_ex(&_0$$3, freetypephp_exception_ce);
		ZEPHIR_INIT_VAR(&_1$$3);
		ZEPHIR_CONCAT_SVS(&_1$$3, "Font file ", &path, " does not exist");
		ZEPHIR_CALL_METHOD(NULL, &_0$$3, "__construct", NULL, 1, &_1$$3);
		zephir_check_call_status();
		zephir_throw_exception_debug(&_0$$3, "freetypephp/font.zep", 21 TSRMLS_CC);
		ZEPHIR_MM_RESTORE();
		return;
	}
	zephir_update_property_zval(this_ptr, SL("_path"), &path);
	ZEPHIR_MM_RESTORE();

}

/**
 * Get an associative array in format array(<char_code> => <glyph_index>)
 * with all glyphs presented in the font.
 *
 * @return array
 */
PHP_METHOD(FreeTypePhp_Font, getAllChars) {

	zval _0;
	ZEPHIR_INIT_THIS();

	ZVAL_UNDEF(&_0);


	zephir_read_property(&_0, this_ptr, SL("_path"), PH_NOISY_CC | PH_READONLY);
	ftutils_get_all_chars(return_value, &_0 TSRMLS_CC);
	return;

}

/**
 * Get glyph index of given char code in the font
 *
 * @param int charCode
 * @return int
 */
PHP_METHOD(FreeTypePhp_Font, getCharIndex) {

	zval *charCode_param = NULL, _0, _1;
	int charCode;
	ZEPHIR_INIT_THIS();

	ZVAL_UNDEF(&_0);
	ZVAL_UNDEF(&_1);

	zephir_fetch_params(0, 1, 0, &charCode_param);

	charCode = zephir_get_intval(charCode_param);


	zephir_read_property(&_0, this_ptr, SL("_path"), PH_NOISY_CC | PH_READONLY);
	ZVAL_LONG(&_1, charCode);
	RETURN_LONG(ftutils_get_char_index(&_0,&_1));

}

/**
 * Get font family name
 *
 * @return string
 */
PHP_METHOD(FreeTypePhp_Font, getFamilyName) {

	zval _0;
	ZEPHIR_INIT_THIS();

	ZVAL_UNDEF(&_0);


	zephir_read_property(&_0, this_ptr, SL("_path"), PH_NOISY_CC | PH_READONLY);
	ftutils_get_family_name(return_value, &_0);
	return;

}

/**
 * Get font style name (Regular, Bold and etc.)
 *
 * @return string
 */
PHP_METHOD(FreeTypePhp_Font, getStyleName) {

	zval _0;
	ZEPHIR_INIT_THIS();

	ZVAL_UNDEF(&_0);


	zephir_read_property(&_0, this_ptr, SL("_path"), PH_NOISY_CC | PH_READONLY);
	ftutils_get_style_name(return_value, &_0);
	return;

}

/**
 * Get glyph indexes needed to render the string by current font
 *
 * @param string inputString
 * @param string langCode
 * @param string textScript
 * @param string textDirection
 * @return array
 */
PHP_METHOD(FreeTypePhp_Font, getStringGlyphs) {

	zval *inputString_param = NULL, *langCode_param = NULL, *textScript_param = NULL, *textDirection_param = NULL, _0;
	zval inputString, langCode, textScript, textDirection;
	ZEPHIR_INIT_THIS();

	ZVAL_UNDEF(&inputString);
	ZVAL_UNDEF(&langCode);
	ZVAL_UNDEF(&textScript);
	ZVAL_UNDEF(&textDirection);
	ZVAL_UNDEF(&_0);

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 2, &inputString_param, &langCode_param, &textScript_param, &textDirection_param);

	zephir_get_strval(&inputString, inputString_param);
	zephir_get_strval(&langCode, langCode_param);
	if (!textScript_param) {
		ZEPHIR_INIT_VAR(&textScript);
		ZVAL_STRING(&textScript, "");
	} else {
		zephir_get_strval(&textScript, textScript_param);
	}
	if (!textDirection_param) {
		ZEPHIR_INIT_VAR(&textDirection);
		ZVAL_STRING(&textDirection, "");
	} else {
		zephir_get_strval(&textDirection, textDirection_param);
	}


	if (ZEPHIR_IS_STRING_IDENTICAL(&textScript, "")) {
		ZEPHIR_INIT_NVAR(&textScript);
		ZVAL_STRING(&textScript, "Latn");
	}
	if (ZEPHIR_IS_STRING_IDENTICAL(&textDirection, "")) {
		ZEPHIR_INIT_NVAR(&textDirection);
		ZVAL_STRING(&textDirection, "ltr");
	}
	zephir_read_property(&_0, this_ptr, SL("_path"), PH_NOISY_CC | PH_READONLY);
	hbutils_get_string_glyphs(return_value, &_0, &inputString, &langCode, &textScript, &textDirection TSRMLS_CC);
	RETURN_MM();

}

/**
 * Get glyph indexes needed to render texts from file by current font
 *
 * @param string filePath
 * @param string langCode
 * @param string textScript
 * @param string textDirection
 * @return array
 */
PHP_METHOD(FreeTypePhp_Font, getFileGlyphs) {

	zval *filePath_param = NULL, *langCode_param = NULL, *textScript_param = NULL, *textDirection_param = NULL, _0;
	zval filePath, langCode, textScript, textDirection;
	ZEPHIR_INIT_THIS();

	ZVAL_UNDEF(&filePath);
	ZVAL_UNDEF(&langCode);
	ZVAL_UNDEF(&textScript);
	ZVAL_UNDEF(&textDirection);
	ZVAL_UNDEF(&_0);

	ZEPHIR_MM_GROW();
	zephir_fetch_params(1, 2, 2, &filePath_param, &langCode_param, &textScript_param, &textDirection_param);

	zephir_get_strval(&filePath, filePath_param);
	zephir_get_strval(&langCode, langCode_param);
	if (!textScript_param) {
		ZEPHIR_INIT_VAR(&textScript);
		ZVAL_STRING(&textScript, "");
	} else {
		zephir_get_strval(&textScript, textScript_param);
	}
	if (!textDirection_param) {
		ZEPHIR_INIT_VAR(&textDirection);
		ZVAL_STRING(&textDirection, "");
	} else {
		zephir_get_strval(&textDirection, textDirection_param);
	}


	if (ZEPHIR_IS_STRING_IDENTICAL(&textScript, "")) {
		ZEPHIR_INIT_NVAR(&textScript);
		ZVAL_STRING(&textScript, "Latn");
	}
	if (ZEPHIR_IS_STRING_IDENTICAL(&textDirection, "")) {
		ZEPHIR_INIT_NVAR(&textDirection);
		ZVAL_STRING(&textDirection, "ltr");
	}
	zephir_read_property(&_0, this_ptr, SL("_path"), PH_NOISY_CC | PH_READONLY);
	hbutils_get_file_glyphs(return_value, &_0, &filePath, &langCode, &textScript, &textDirection TSRMLS_CC);
	RETURN_MM();

}

