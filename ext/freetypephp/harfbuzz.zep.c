
#ifdef HAVE_CONFIG_H
#include "../ext_config.h"
#endif

#include <php.h>
#include "../php_ext.h"
#include "../ext.h"

#include <Zend/zend_operators.h>
#include <Zend/zend_exceptions.h>
#include <Zend/zend_interfaces.h>

#include "kernel/main.h"


/**
 * HarfBuzz constants & etc.
 */
ZEPHIR_INIT_CLASS(FreeTypePhp_HarfBuzz) {

	ZEPHIR_REGISTER_CLASS(FreeTypePhp, HarfBuzz, freetypephp, harfbuzz, NULL, 0);

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_COMMON"), "Zyyy");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_INHERITED"), "Zinh");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_UNKNOWN"), "Zzzz");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_ARABIC"), "Arab");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_ARMENIAN"), "Armn");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BENGALI"), "Beng");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_CYRILLIC"), "Cyrl");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_DEVANAGARI"), "Deva");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_GEORGIAN"), "Geor");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_GREEK"), "Grek");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_GUJARATI"), "Gujr");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_GURMUKHI"), "Guru");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_HANGUL"), "Hang");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_HAN"), "Hani");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_HEBREW"), "Hebr");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_HIRAGANA"), "Hira");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_KANNADA"), "Knda");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_KATAKANA"), "Kana");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_LAO"), "Laoo");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_LATIN"), "Latn");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MALAYALAM"), "Mlym");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_ORIYA"), "Orya");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TAMIL"), "Taml");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TELUGU"), "Telu");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_THAI"), "Thai");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TIBETAN"), "Tibt");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BOPOMOFO"), "Bopo");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BRAILLE"), "Brai");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_CANADIAN_SYLLABICS"), "Cans");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_CHEROKEE"), "Cher");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_ETHIOPIC"), "Ethi");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_KHMER"), "Khmr");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MONGOLIAN"), "Mong");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MYANMAR"), "Mymr");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OGHAM"), "Ogam");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_RUNIC"), "Runr");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SINHALA"), "Sinh");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SYRIAC"), "Syrc");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_THAANA"), "Thaa");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_YI"), "Yiii");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_DESERET"), "Dsrt");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_GOTHIC"), "Goth");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OLD_ITALIC"), "Ital");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BUHID"), "Buhd");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_HANUNOO"), "Hano");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TAGALOG"), "Tglg");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TAGBANWA"), "Tagb");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_CYPRIOT"), "Cprt");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_LIMBU"), "Limb");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_LINEAR_B"), "Linb");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OSMANYA"), "Osma");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SHAVIAN"), "Shaw");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TAI_LE"), "Tale");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_UGARITIC"), "Ugar");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BUGINESE"), "Bugi");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_COPTIC"), "Copt");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_GLAGOLITIC"), "Glag");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_KHAROSHTHI"), "Khar");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_NEW_TAI_LUE"), "Talu");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OLD_PERSIAN"), "Xpeo");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SYLOTI_NAGRI"), "Sylo");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TIFINAGH"), "Tfng");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BALINESE"), "Bali");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_CUNEIFORM"), "Xsux");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_NKO"), "Nkoo");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_PHAGS_PA"), "Phag");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_PHOENICIAN"), "Phnx");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_CARIAN"), "Cari");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_CHAM"), "Cham");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_KAYAH_LI"), "Kali");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_LEPCHA"), "Lepc");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_LYCIAN"), "Lyci");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_LYDIAN"), "Lydi");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OL_CHIKI"), "Olck");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_REJANG"), "Rjng");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SAURASHTRA"), "Saur");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SUNDANESE"), "Sund");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_VAI"), "Vaii");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_AVESTAN"), "Avst");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BAMUM"), "Bamu");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_EGYPTIAN_HIEROGLYPHS"), "Egyp");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_IMPERIAL_ARAMAIC"), "Armi");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_INSCRIPTIONAL_PAHLAVI"), "Phli");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_INSCRIPTIONAL_PARTHIAN"), "Prti");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_JAVANESE"), "Java");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_KAITHI"), "Kthi");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_LISU"), "Lisu");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MEETEI_MAYEK"), "Mtei");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OLD_SOUTH_ARABIAN"), "Sarb");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OLD_TURKIC"), "Orkh");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SAMARITAN"), "Samr");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TAI_THAM"), "Lana");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TAI_VIET"), "Tavt");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BATAK"), "Batk");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BRAHMI"), "Brah");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MANDAIC"), "Mand");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_CHAKMA"), "Cakm");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MEROITIC_CURSIVE"), "Merc");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MEROITIC_HIEROGLYPHS"), "Mero");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MIAO"), "Plrd");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SHARADA"), "Shrd");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SORA_SOMPENG"), "Sora");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TAKRI"), "Takr");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BASSA_VAH"), "Bass");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_CAUCASIAN_ALBANIAN"), "Aghb");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_DUPLOYAN"), "Dupl");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_ELBASAN"), "Elba");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_GRANTHA"), "Gran");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_KHOJKI"), "Khoj");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_KHUDAWADI"), "Sind");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_LINEAR_A"), "Lina");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MAHAJANI"), "Mahj");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MANICHAEAN"), "Mani");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MENDE_KIKAKUI"), "Mend");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MODI"), "Modi");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MRO"), "Mroo");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_NABATAEAN"), "Nbat");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OLD_NORTH_ARABIAN"), "Narb");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OLD_PERMIC"), "Perm");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_PAHAWH_HMONG"), "Hmng");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_PALMYRENE"), "Palm");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_PAU_CIN_HAU"), "Pauc");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_PSALTER_PAHLAVI"), "Phlp");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SIDDHAM"), "Sidd");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TIRHUTA"), "Tirh");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_WARANG_CITI"), "Wara");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_AHOM"), "Ahom");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_ANATOLIAN_HIEROGLYPHS"), "Hluw");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_HATRAN"), "Hatr");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MULTANI"), "Mult");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OLD_HUNGARIAN"), "Hung");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_SIGNWRITING"), "Sgnw");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_ADLAM"), "Adlm");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_BHAIKSUKI"), "Bhks");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_MARCHEN"), "Marc");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_OSAGE"), "Osge");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_TANGUT"), "Tang");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_NEWA"), "Newa");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("SCRIPT_DEFAULT"), "Latn");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("DIRECTION_LTR"), "ltr");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("DIRECTION_RTL"), "rtl");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("DIRECTION_TTB"), "ttb");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("DIRECTION_BTT"), "btt");

	zephir_declare_class_constant_string(freetypephp_harfbuzz_ce, SL("DIRECTION_DEFAULT"), "ltr");

	return SUCCESS;

}

