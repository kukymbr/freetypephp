
extern zend_class_entry *freetypephp_font_ce;

ZEPHIR_INIT_CLASS(FreeTypePhp_Font);

PHP_METHOD(FreeTypePhp_Font, __construct);
PHP_METHOD(FreeTypePhp_Font, getAllChars);
PHP_METHOD(FreeTypePhp_Font, getCharIndex);
PHP_METHOD(FreeTypePhp_Font, getFamilyName);
PHP_METHOD(FreeTypePhp_Font, getStyleName);
PHP_METHOD(FreeTypePhp_Font, getStringGlyphs);
PHP_METHOD(FreeTypePhp_Font, getFileGlyphs);

ZEND_BEGIN_ARG_INFO_EX(arginfo_freetypephp_font___construct, 0, 0, 1)
	ZEND_ARG_INFO(0, path)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_freetypephp_font_getcharindex, 0, 0, 1)
	ZEND_ARG_INFO(0, charCode)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_freetypephp_font_getstringglyphs, 0, 0, 2)
	ZEND_ARG_INFO(0, inputString)
	ZEND_ARG_INFO(0, langCode)
	ZEND_ARG_INFO(0, textScript)
	ZEND_ARG_INFO(0, textDirection)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(arginfo_freetypephp_font_getfileglyphs, 0, 0, 2)
	ZEND_ARG_INFO(0, filePath)
	ZEND_ARG_INFO(0, langCode)
	ZEND_ARG_INFO(0, textScript)
	ZEND_ARG_INFO(0, textDirection)
ZEND_END_ARG_INFO()

ZEPHIR_INIT_FUNCS(freetypephp_font_method_entry) {
	PHP_ME(FreeTypePhp_Font, __construct, arginfo_freetypephp_font___construct, ZEND_ACC_PUBLIC|ZEND_ACC_CTOR)
	PHP_ME(FreeTypePhp_Font, getAllChars, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(FreeTypePhp_Font, getCharIndex, arginfo_freetypephp_font_getcharindex, ZEND_ACC_PUBLIC)
	PHP_ME(FreeTypePhp_Font, getFamilyName, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(FreeTypePhp_Font, getStyleName, NULL, ZEND_ACC_PUBLIC)
	PHP_ME(FreeTypePhp_Font, getStringGlyphs, arginfo_freetypephp_font_getstringglyphs, ZEND_ACC_PUBLIC)
	PHP_ME(FreeTypePhp_Font, getFileGlyphs, arginfo_freetypephp_font_getfileglyphs, ZEND_ACC_PUBLIC)
	PHP_FE_END
};
