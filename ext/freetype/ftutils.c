/**
 * Funtions to get true type font data 
 * by FreeType library 
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ext.h"
#include "zend_exceptions.h"

#include <ft2build.h>
#include FT_FREETYPE_H

#include <stdio.h>
#include <unistd.h>

/**
 * Initialize font library and face by font path
 */
unsigned int ftutils_init_font(zval *fontPath, FT_Library *oFTLibrary, FT_Face *oFTFace) {

	if (Z_TYPE_P(fontPath) != IS_STRING) {
		convert_to_string(fontPath);
	}
	char* path = Z_STRVAL_P(fontPath);

	if (access (path, F_OK) == -1 ) {
		return 1;
	}

	if (FT_Init_FreeType( oFTLibrary )) {
		return 2;
	}

	if (FT_New_Face( *oFTLibrary, path, 0, oFTFace )) {
		return 3;
	}

	return 0;
}

/**
 * Make a PHP associative array in format array(<char_code> => <glyph_index>)
 * with all glyphs presented in the font path
 */
void ftutils_get_all_chars(zval *return_value, zval *fontPath) {

	FT_Library oFTLibrary;
	FT_Face oFTFace;

	unsigned int error;
	error = ftutils_init_font(fontPath, &oFTLibrary, &oFTFace);

	if (error) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "Cannot init font. Error code %u", error);
		return;
	}

	FT_Select_Charmap(oFTFace, FT_ENCODING_UNICODE);

	unsigned long charcode;
	unsigned int gindex;
	unsigned int i;

	array_init(return_value);

	charcode = FT_Get_First_Char( oFTFace, &gindex );

	int j = 0;
	while ( 1 ) {
		if (gindex != 0) {
			add_index_long(return_value, charcode, gindex);
		} else {
			break;
		}
		charcode = FT_Get_Next_Char( oFTFace, charcode, &gindex );
	}

	FT_Done_Face( oFTFace );
	FT_Done_FreeType( oFTLibrary );
}

/**
 * Get glyph index of given char code in font path
 */
unsigned int ftutils_get_char_index(zval *fontPath, zval *zCharCode) {

	FT_Library oFTLibrary;
	FT_Face oFTFace;

	unsigned int error;
	error = ftutils_init_font(fontPath, &oFTLibrary, &oFTFace);

	if (error) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "Cannot init font. Error code %u", error);
		return 0;
	}

	FT_Select_Charmap(oFTFace, FT_ENCODING_UNICODE);

	long charCode = Z_LVAL_P(zCharCode);

	unsigned int nGlyphIndex = FT_Get_Char_Index( oFTFace, (FT_UInt) charCode);

	FT_Done_Face( oFTFace );
	FT_Done_FreeType( oFTLibrary );

	return nGlyphIndex;
}

/**
 * Get font family name
 */
char* ftutils_get_family_name(zval *return_value, zval *fontPath) {

	FT_Library oFTLibrary;
	FT_Face oFTFace;

	unsigned int error;
	error = ftutils_init_font(fontPath, &oFTLibrary, &oFTFace);

	if (error) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "Cannot init font. Error code %u", error);
		return;
	}

	ZVAL_STRING(return_value, oFTFace->family_name);
}

/**
 * Get font style name
 */
char* ftutils_get_style_name(zval *return_value, zval *fontPath) {

	FT_Library oFTLibrary;
	FT_Face oFTFace;

	unsigned int error;
	error = ftutils_init_font(fontPath, &oFTLibrary, &oFTFace);

	if (error) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "Cannot init font. Error code %u", error);
		return;
	}

	ZVAL_STRING(return_value, oFTFace->style_name);
}