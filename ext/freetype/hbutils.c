/**
 * Functions to get true type font data
 * by HarfBuzz library 
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ext.h"
#include "zend_exceptions.h"

#include <stdlib.h>
#include <stdio.h>

#include <hb.h>
#include <hb-ft.h>
#include <hb-icu.h>

/**
 * Convert zval to string
 */
char* hbutils_zval_to_string(zval *value) {

	if (Z_TYPE_P(value) != IS_STRING) {
		convert_to_string(value);
	}

	return Z_STRVAL_P(value);
}

/**
 * Init text direction from input (ltr, rtl, ttb, btt)
 */
int hbutils_input_direction(zval *inputDirection, hb_direction_t *direction) {

	char* directionStr = hbutils_zval_to_string(inputDirection);
	*direction = hb_direction_from_string(directionStr, strlen(directionStr));

	if (*direction == HB_DIRECTION_INVALID) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "Invalid direction given: %s", directionStr);
		return 1;
	}

	return 0;
}

/**
 * Init text script from input (e. g. "Latn")
 */
int hbutils_input_script(zval *inputScript, hb_script_t *script) {

	char* scriptStr = hbutils_zval_to_string(inputScript);	
	*script = hb_script_from_string(scriptStr, strlen(scriptStr));

	if (*script == HB_SCRIPT_INVALID) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "Invalid script given: %s", scriptStr);
		return 1;
	}

	return 0;
}

/**
 * Init language from input (e. g. "en")
 */
int hbutils_input_lang(zval *inputLang, hb_language_t *lang) {

	char* langStr = hbutils_zval_to_string(inputLang);
	*lang = hb_language_from_string (langStr, strlen(langStr));

	if (*lang == HB_LANGUAGE_INVALID) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "Invalid language given: %s", langStr);
		return 1;
	}

	return 0;
}

/**
 * Get all glyphs in string and put them to initialized php array
 */
void hbutils_get_string_glyphs_to_array(zval *return_value, char* path, char* inputStr, hb_direction_t direction, hb_script_t script, hb_language_t lang) {

	const double ptSize = 50.0;
    const int device_hdpi = 100;
    const int device_vdpi = 100;
    const int nLength = strlen(inputStr);

    FT_Library ft_library;
    FT_Init_FreeType(&ft_library);

	FT_Face ft_face;
    FT_New_Face(ft_library, path, 0, &ft_face);
    FT_Set_Char_Size(ft_face, 0, ptSize, device_hdpi, device_vdpi);

    hb_font_t *hb_ft_font;
    hb_face_t *hb_ft_face;
    hb_ft_font = hb_ft_font_create(ft_face, NULL);
    hb_ft_face = hb_ft_face_create(ft_face, NULL);

    hb_buffer_t *buf = hb_buffer_create();

    hb_buffer_set_unicode_funcs(buf, hb_icu_get_unicode_funcs());
    hb_buffer_set_direction(buf, direction);
    hb_buffer_set_script(buf, script);
    hb_buffer_set_language(buf, lang);

    hb_buffer_set_content_type( buf, HB_BUFFER_CONTENT_TYPE_UNICODE );
    hb_buffer_add_utf8( buf, inputStr, nLength, 0, nLength );
    hb_buffer_guess_segment_properties( buf );
    hb_shape(hb_ft_font, buf, NULL, 0);

    unsigned int glyph_count;
    hb_glyph_info_t *glyph_info = hb_buffer_get_glyph_infos(buf, &glyph_count);
    hb_glyph_position_t *glyph_pos = hb_buffer_get_glyph_positions(buf, &glyph_count);

	int it;

    for (it = 0; it < glyph_count; it++)
    {
        int glyph = glyph_info[it].codepoint;
        if (glyph) {
            add_index_long(return_value, glyph, glyph);
        }
    }

    hb_buffer_destroy(buf);
    hb_font_destroy(hb_ft_font);
    hb_face_destroy(hb_ft_face);
    FT_Done_FreeType(ft_library);
}

/**
 * Make PHP array with all glyphs containing in the given string
 */
void hbutils_get_string_glyphs(zval *return_value, zval *inputFontPath, zval *inputString, zval *inputLang, zval *inputScript, zval *inputDirection) {

	// Path to font file
	const char* path = hbutils_zval_to_string(inputFontPath);
	if (access (path, F_OK) == -1 ) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "Font path does not exist: %s", path);
		return;
	}

	// String to process
	const char* inputStr = hbutils_zval_to_string(inputString);

	// Text direction
	const hb_direction_t direction;
	if (hbutils_input_direction(inputDirection, &direction)) {
		return;
	}

	// Text script
	const hb_script_t script;
	if (hbutils_input_script(inputScript, &script)) {
		return;
	}

	// Language
	const hb_language_t lang;
	if (hbutils_input_lang(inputLang, &lang)) {
		return;
	}

	array_init(return_value);

	hbutils_get_string_glyphs_to_array(return_value, path, inputStr, direction, script, lang);
}

/**
 * Make PHP array with all glyphs containing in the file
 */
void hbutils_get_file_glyphs(zval *return_value, zval *inputFontPath, zval *inputFile, zval *inputLang, zval *inputScript, zval *inputDirection) {

	// Path to font file
	const char* path = hbutils_zval_to_string(inputFontPath);
	if (access (path, F_OK) == -1 ) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "Font path does not exist: %s", path);
		return;
	}

	// File to process
	const char* filePath = hbutils_zval_to_string(inputFile);
	if (access (filePath, F_OK) == -1 ) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "File does not exist: %s", filePath);
		return;
	}

	// Text direction
	const hb_direction_t direction;
	if (hbutils_input_direction(inputDirection, &direction)) {
		return;
	}

	// Text script
	const hb_script_t script;
	if (hbutils_input_script(inputScript, &script)) {
		return;
	}

	// Language
	const hb_language_t lang;
	if (hbutils_input_lang(inputLang, &lang)) {
		return;
	}

	FILE *fp = fopen(filePath , "r");

	if (fp == NULL) {
		zend_throw_exception_ex(NULL, 0 TSRMLS_DC, "Failed to open file: %s", filePath);
		return;
	}

	array_init(return_value);

	char* line = NULL;
	size_t len = 0;
	char read;

	while (read = getline(&line , &len, fp) != -1) {
		hbutils_get_string_glyphs_to_array(return_value, path, line, direction, script, lang);
	}
}