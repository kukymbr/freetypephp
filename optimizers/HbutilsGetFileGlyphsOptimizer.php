<?php

namespace Zephir\Optimizers\FunctionCall;

use Zephir\Call;
use Zephir\CompiledExpression;
use Zephir\CompilerException;
use Zephir\CompilationContext;
use Zephir\Optimizers\OptimizerAbstract;

/**
 * Class HbutilsGetFileGlyphsOptimizer.
 *
 * @package Zephir\Optimizers\FunctionCall
 */
class HbutilsGetFileGlyphsOptimizer extends OptimizerAbstract
{
	public function optimize(array $expression, Call $call, CompilationContext $context)
	{
		if (!isset($expression['parameters']) || count($expression['parameters']) != 5) {
			throw new CompilerException('Invalid parameters count in hbutils_get_file_glyphs() call');
		}

		$call->processExpectedReturn($context);

		$symbolVariable = $call->getSymbolVariable(true, $context);

		if (!$symbolVariable->isVariable()) {
			throw new CompilerException("Returned values by functions can only be assigned to variant variables", $expression);
		}

		$symbolVariable->setDynamicTypes('array');

		$resolvedParams = $call->getReadOnlyResolvedParams($expression['parameters'], $context, $expression);

		if ($call->mustInitSymbolVariable()) {
			$symbolVariable->initVariant($context);
		}

		$symbol = $context->backend->getVariableCode($symbolVariable);

		$context->codePrinter->output('hbutils_get_file_glyphs(' . $symbol . ', ' . implode(', ', array_slice($resolvedParams, 0, 5)) . ' TSRMLS_CC);');

		return new CompiledExpression('variable', $symbolVariable->getRealName(), $expression);
	}
}