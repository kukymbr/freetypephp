<?php

namespace Zephir\Optimizers\FunctionCall;

use Zephir\Call;
use Zephir\CompiledExpression;
use Zephir\CompilerException;
use Zephir\CompilationContext;
use Zephir\Optimizers\OptimizerAbstract;

/**
 * Class FtutilsGetCharIndexOptimizer.
 *
 * @package Zephir\Optimizers\FunctionCall
 */
class FtutilsGetAllCharsOptimizer extends OptimizerAbstract
{
	public function optimize(array $expression, Call $call, CompilationContext $context)
	{
		if (!isset($expression['parameters']) || count($expression['parameters']) != 1) {
			throw new CompilerException('Invalid parameters count in ftutils_get_all_chars() call');
		}

		$call->processExpectedReturn($context);

		$symbolVariable = $call->getSymbolVariable(true, $context);

		if (!$symbolVariable->isVariable()) {
			throw new CompilerException("Returned values by functions can only be assigned to variant variables", $expression);
		}

		$symbolVariable->setDynamicTypes('array');

		$resolvedParams = $call->getReadOnlyResolvedParams($expression['parameters'], $context, $expression);

		if ($call->mustInitSymbolVariable()) {
			$symbolVariable->initVariant($context);
		}

		$symbol = $context->backend->getVariableCode($symbolVariable);

		$context->codePrinter->output('ftutils_get_all_chars(' . $symbol . ', ' . $resolvedParams[0] . ' TSRMLS_CC);');

		return new CompiledExpression('variable', $symbolVariable->getRealName(), $expression);
	}
}