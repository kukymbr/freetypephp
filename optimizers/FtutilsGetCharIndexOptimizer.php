<?php

namespace Zephir\Optimizers\FunctionCall;

use Zephir\Call;
use Zephir\CompiledExpression;
use Zephir\CompilerException;
use Zephir\CompilationContext;
use Zephir\Optimizers\OptimizerAbstract;

/**
 * Class FtutilsGetCharIndexOptimizer.
 *
 * @package Zephir\Optimizers\FunctionCall
 */
class FtutilsGetCharIndexOptimizer extends OptimizerAbstract
{
	public function optimize(array $expression, Call $call, CompilationContext $context)
	{
		if (!isset($expression['parameters']) || count($expression['parameters']) != 2) {
			throw new CompilerException('Invalid parameters count in ftutils_get_char_index() call');
		}

		$resolvedParams = $call->getReadOnlyResolvedParams($expression['parameters'], $context, $expression);

		return new CompiledExpression(
			'int',
			'ftutils_get_char_index(' . $resolvedParams[0] . ',' . $resolvedParams[1] . ')',
			$expression
		);
	}
}