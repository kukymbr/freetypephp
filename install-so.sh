#!/bin/sh

file=/etc/php/7.0/mods-available/50-freetypephp.ini;
link=/etc/php/7.0/apache2/conf.d/50-freetypephp.ini;

echo "; FreeType for PHP extension\nextension=freetypephp.so" > "$file";

if [ ! -L "$link" ]
then
	ln -s "$file" "$link";
fi;

link=/etc/php/7.0/cli/conf.d/50-freetypephp.ini;

if [ ! -L "$link" ]
then
	ln -s "$file" "$link";
fi;

service apache2 restart;

echo "Done";