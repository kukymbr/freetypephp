namespace FreeTypePhp;

/**
 * TrueType Font processing class
 */
class Font
{
	/**
	 * Path to font file
	 */
	protected _path = null;

	/**
	 * Constructor
	 *
	 * @param string path Path to font file
	 */
	public function __construct(string! path)
	{
		if (!file_exists(path)) {
			throw new Exception("Font file " . path . " does not exist");
		}

		let this->_path = path;
	}

	/**
	 * Get an associative array in format array(<char_code> => <glyph_index>)
     * with all glyphs presented in the font.
     *
     * @return array
	 */
	public function getAllChars() -> array
	{
		return ftutils_get_all_chars(this->_path);
	}

	/**
	 * Get glyph index of given char code in the font
	 *
	 * @param int charCode
	 * @return int
	 */
	public function getCharIndex(int charCode) -> int
	{
		return ftutils_get_char_index(this->_path, charCode);
	}

	/**
	 * Get font family name
	 *
	 * @return string
	 */
	public function getFamilyName() -> string
	{
		return ftutils_get_family_name(this->_path);
	}

	/**
     * Get font style name (Regular, Bold and etc.)
     *
     * @return string
     */
    public function getStyleName() -> string
    {
        return ftutils_get_style_name(this->_path);
    }

	/**
	 * Get glyph indexes needed to render the string by current font
	 *
	 * @param string inputString
	 * @param string langCode
	 * @param string textScript
	 * @param string textDirection
	 * @return array
	 */ 
    public function getStringGlyphs(const string inputString, const string langCode, string textScript = null, string textDirection = null) -> array
    {
		if (textScript === null) {
			let textScript = \FreeTypePhp\HarfBuzz::SCRIPT_DEFAULT;
		}

		if (textDirection === null) {
			let textDirection = \FreeTypePhp\HarfBuzz::DIRECTION_DEFAULT;
		}

    	return hbutils_get_string_glyphs(this->_path, inputString, langCode, textScript, textDirection);
    }

    /**
     * Get glyph indexes needed to render texts from file by current font
     *
     * @param string filePath
     * @param string langCode
     * @param string textScript
     * @param string textDirection
     * @return array
     */
    public function getFileGlyphs(const string filePath, const string langCode, string textScript = null, string textDirection = null) -> array
    {
        if (textScript === null) {
            let textScript = \FreeTypePhp\HarfBuzz::SCRIPT_DEFAULT;
        }

        if (textDirection === null) {
            let textDirection = \FreeTypePhp\HarfBuzz::DIRECTION_DEFAULT;
        }

        return hbutils_get_file_glyphs(this->_path, filePath, langCode, textScript, textDirection);
    }
}