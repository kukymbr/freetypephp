namespace FreeTypePhp;

/**
 * HarfBuzz constants & etc.
 */
class HarfBuzz
{
	const SCRIPT_COMMON = "Zyyy";
    const SCRIPT_INHERITED = "Zinh";
    const SCRIPT_UNKNOWN = "Zzzz";

    const SCRIPT_ARABIC = "Arab";
    const SCRIPT_ARMENIAN = "Armn";
    const SCRIPT_BENGALI = "Beng";
    const SCRIPT_CYRILLIC = "Cyrl";
    const SCRIPT_DEVANAGARI = "Deva";
    const SCRIPT_GEORGIAN = "Geor";
    const SCRIPT_GREEK = "Grek";
    const SCRIPT_GUJARATI = "Gujr";
    const SCRIPT_GURMUKHI = "Guru";
    const SCRIPT_HANGUL = "Hang";
    const SCRIPT_HAN = "Hani";
    const SCRIPT_HEBREW = "Hebr";
    const SCRIPT_HIRAGANA = "Hira";
    const SCRIPT_KANNADA = "Knda";
    const SCRIPT_KATAKANA = "Kana";
    const SCRIPT_LAO = "Laoo";
    const SCRIPT_LATIN = "Latn";
    const SCRIPT_MALAYALAM = "Mlym";
    const SCRIPT_ORIYA = "Orya";
    const SCRIPT_TAMIL = "Taml";
    const SCRIPT_TELUGU = "Telu";
    const SCRIPT_THAI = "Thai";

    const SCRIPT_TIBETAN = "Tibt";

    const SCRIPT_BOPOMOFO = "Bopo";
    const SCRIPT_BRAILLE = "Brai";
    const SCRIPT_CANADIAN_SYLLABICS = "Cans";
    const SCRIPT_CHEROKEE = "Cher";
    const SCRIPT_ETHIOPIC = "Ethi";
    const SCRIPT_KHMER = "Khmr";
    const SCRIPT_MONGOLIAN = "Mong";
    const SCRIPT_MYANMAR = "Mymr";
    const SCRIPT_OGHAM = "Ogam";
    const SCRIPT_RUNIC = "Runr";
    const SCRIPT_SINHALA = "Sinh";
    const SCRIPT_SYRIAC = "Syrc";
    const SCRIPT_THAANA = "Thaa";
    const SCRIPT_YI = "Yiii";

    const SCRIPT_DESERET = "Dsrt";
    const SCRIPT_GOTHIC = "Goth";
    const SCRIPT_OLD_ITALIC = "Ital";

    const SCRIPT_BUHID = "Buhd";
    const SCRIPT_HANUNOO = "Hano";
    const SCRIPT_TAGALOG = "Tglg";
    const SCRIPT_TAGBANWA = "Tagb";

    const SCRIPT_CYPRIOT = "Cprt";
    const SCRIPT_LIMBU = "Limb";
    const SCRIPT_LINEAR_B = "Linb";
    const SCRIPT_OSMANYA = "Osma";
    const SCRIPT_SHAVIAN = "Shaw";
    const SCRIPT_TAI_LE = "Tale";
    const SCRIPT_UGARITIC = "Ugar";

    const SCRIPT_BUGINESE = "Bugi";
    const SCRIPT_COPTIC = "Copt";
    const SCRIPT_GLAGOLITIC = "Glag";
    const SCRIPT_KHAROSHTHI = "Khar";
    const SCRIPT_NEW_TAI_LUE = "Talu";
    const SCRIPT_OLD_PERSIAN = "Xpeo";
    const SCRIPT_SYLOTI_NAGRI = "Sylo";
    const SCRIPT_TIFINAGH = "Tfng";

    const SCRIPT_BALINESE = "Bali";
    const SCRIPT_CUNEIFORM = "Xsux";
    const SCRIPT_NKO = "Nkoo";
    const SCRIPT_PHAGS_PA = "Phag";
    const SCRIPT_PHOENICIAN = "Phnx";

    const SCRIPT_CARIAN = "Cari";
    const SCRIPT_CHAM = "Cham";
    const SCRIPT_KAYAH_LI = "Kali";
    const SCRIPT_LEPCHA = "Lepc";
    const SCRIPT_LYCIAN = "Lyci";
    const SCRIPT_LYDIAN = "Lydi";
    const SCRIPT_OL_CHIKI = "Olck";
    const SCRIPT_REJANG = "Rjng";
    const SCRIPT_SAURASHTRA = "Saur";
    const SCRIPT_SUNDANESE = "Sund";
    const SCRIPT_VAI = "Vaii";

    const SCRIPT_AVESTAN = "Avst";
    const SCRIPT_BAMUM = "Bamu";
    const SCRIPT_EGYPTIAN_HIEROGLYPHS = "Egyp";
    const SCRIPT_IMPERIAL_ARAMAIC = "Armi";
    const SCRIPT_INSCRIPTIONAL_PAHLAVI = "Phli";
    const SCRIPT_INSCRIPTIONAL_PARTHIAN = "Prti";
    const SCRIPT_JAVANESE = "Java";
    const SCRIPT_KAITHI = "Kthi";
    const SCRIPT_LISU = "Lisu";
    const SCRIPT_MEETEI_MAYEK = "Mtei";
    const SCRIPT_OLD_SOUTH_ARABIAN = "Sarb";
    const SCRIPT_OLD_TURKIC = "Orkh";
    const SCRIPT_SAMARITAN = "Samr";
    const SCRIPT_TAI_THAM = "Lana";
    const SCRIPT_TAI_VIET = "Tavt";

    const SCRIPT_BATAK = "Batk";
    const SCRIPT_BRAHMI = "Brah";
    const SCRIPT_MANDAIC = "Mand";

    const SCRIPT_CHAKMA = "Cakm";
    const SCRIPT_MEROITIC_CURSIVE = "Merc";
    const SCRIPT_MEROITIC_HIEROGLYPHS = "Mero";
    const SCRIPT_MIAO = "Plrd";
    const SCRIPT_SHARADA = "Shrd";
    const SCRIPT_SORA_SOMPENG = "Sora";
    const SCRIPT_TAKRI = "Takr";

    const SCRIPT_BASSA_VAH = "Bass";
    const SCRIPT_CAUCASIAN_ALBANIAN = "Aghb";
    const SCRIPT_DUPLOYAN = "Dupl";
    const SCRIPT_ELBASAN = "Elba";
    const SCRIPT_GRANTHA = "Gran";
    const SCRIPT_KHOJKI = "Khoj";
    const SCRIPT_KHUDAWADI = "Sind";
    const SCRIPT_LINEAR_A = "Lina";
    const SCRIPT_MAHAJANI = "Mahj";
    const SCRIPT_MANICHAEAN = "Mani";
    const SCRIPT_MENDE_KIKAKUI = "Mend";
    const SCRIPT_MODI = "Modi";
    const SCRIPT_MRO = "Mroo";
    const SCRIPT_NABATAEAN = "Nbat";
    const SCRIPT_OLD_NORTH_ARABIAN = "Narb";
    const SCRIPT_OLD_PERMIC = "Perm";
    const SCRIPT_PAHAWH_HMONG = "Hmng";
    const SCRIPT_PALMYRENE = "Palm";
    const SCRIPT_PAU_CIN_HAU = "Pauc";
    const SCRIPT_PSALTER_PAHLAVI = "Phlp";
    const SCRIPT_SIDDHAM = "Sidd";
    const SCRIPT_TIRHUTA = "Tirh";
    const SCRIPT_WARANG_CITI = "Wara";

    const SCRIPT_AHOM = "Ahom";
    const SCRIPT_ANATOLIAN_HIEROGLYPHS = "Hluw";
    const SCRIPT_HATRAN = "Hatr";
    const SCRIPT_MULTANI = "Mult";
    const SCRIPT_OLD_HUNGARIAN = "Hung";
    const SCRIPT_SIGNWRITING = "Sgnw";

    const SCRIPT_ADLAM = "Adlm";
    const SCRIPT_BHAIKSUKI = "Bhks";
    const SCRIPT_MARCHEN = "Marc";
    const SCRIPT_OSAGE = "Osge";
    const SCRIPT_TANGUT = "Tang";
    const SCRIPT_NEWA = "Newa";

    const SCRIPT_DEFAULT = HarfBuzz::SCRIPT_LATIN;

    const DIRECTION_LTR = "ltr";
    const DIRECTION_RTL = "rtl";
    const DIRECTION_TTB = "ttb";
    const DIRECTION_BTT = "btt";

    const DIRECTION_DEFAULT = HarfBuzz::DIRECTION_LTR;
}